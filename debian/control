Source: libhtp
Priority: optional
Section: libs
Maintainer: Sascha Steinbiss <satta@debian.org>
Build-Depends: debhelper-compat (= 13),
               libz-dev
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: http://openinfosecfoundation.org/
Vcs-Git: https://salsa.debian.org/pkg-suricata-team/pkg-libhtp.git
Vcs-Browser: https://salsa.debian.org/pkg-suricata-team/pkg-libhtp

Package: libhtp-dev
Section: libdevel
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: libhtp2 (= ${binary:Version}), ${misc:Depends}
Description: HTTP normalizer and parser library (devel)
 The HTP Library is an HTTP normalizer and parser.  This integrates and
 provides very advanced processing of HTTP streams for Suricata. The HTP
 library is required by the engine, but may also be used independently in a
 range of applications and tools.
 .
 This package provides the development files for libhtp.

Package: libhtp2
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: HTTP normalizer and parser library
 The HTP Library is an HTTP normalizer and parser.  This integrates and
 provides very advanced processing of HTTP streams for Suricata. The HTP
 library is required by the engine, but may also be used independently in a
 range of applications and tools.
 .
 This package provides the runtime files for libhtp.
